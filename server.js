// load the express package and create our app
 var express = require('express');
 var mongoose = require('mongoose');
 var app = express();
 var path = require('path');
var config = require('./config');

 //database connection

    mongoose.connect(config.db, function (err) {
       if(err){
           console.log(err);
       }
        else {
           console.log('Connected to mongolab');
       }
    });

 // send our index.html file to the user for the home page
 app.get('/', function(req, res) {
     res.sendFile(path.join(__dirname + '/index.html'));
     });

    var adminRouter = express.Router();

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!


    //middleware to log time of every request made via adminRouter

    adminRouter.use(function (req, res, next) {
       console.log('Time : ' + dd + '/' + mm);
       console.log('Method : ' + req.method);
        next();
    });

    adminRouter.get('/', function (req, res) {
        res.send('this is admin home route');
    });

    adminRouter.get('/users', function (req,res) {
        res.send('route for showing all the users');
    });

    // this middleware is route specific, only for /post route, it will run 2 times
    //one with common adminRouter middleware and one specific for /post

    adminRouter.use('/post', function (req, res, next) {
       console.log('request type for post : ' + req.method);
        next();
    });

    adminRouter.get('/post', function (req, res) {
       res.send('route to show all the posts');
    });

    // middleware for logging to console for route for params

    adminRouter.use('/users/:name', function (req, res, next) {
        console.log('Hello ' + req.params.name + '!');
        next();
    });

    // another method of express.router(), param for dealing with params
    adminRouter.param('name', function (req, res, next, name) {
       console.log('hello :' + name);
        next();
    });

    //adding route to deal with parameters passed via url

    adminRouter.get('/users/:name', function (req, res) {
        res.send('Hello ' + req.params.name + ' !');
    });



    adminRouter.get('*',function (req, res){
       res.send('default admin route');
    });


    // all routes defined with adminRouter will start with /admin .
    app.use('/admin', adminRouter);
 // start the server
 app.listen(config.PORT);
 console.log('3000 is the magic port!');

